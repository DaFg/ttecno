var express = require('express');
var pgClient = require('../utils/database_connection');
var router = express.Router();

/* GET home page. */
router.get('/index', function (req, res, next) { //eslint-disable-line no-unused-vars
    var resultado = [];
    console.log(req, res, next);
    var client = pgClient.connect();
    var query = client.query("SELECT * FROM points ORDER BY id ASC;");
    query.on('row', function (row) {
        resultado.push(row);
    });
    query.on('end', function () {
        console.log(arguments);
        client.end();
        return res.json(resultado);
    });

    //res.render('index', {title: 'Express',});
})
;
module.exports = router;
